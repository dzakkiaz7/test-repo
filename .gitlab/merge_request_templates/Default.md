## What does this MR do?

(Summary what does this MR do. example: Migration to Typescript of catalog page)

## Scope of Work

(List tickets related of this MR. example: [NV-1166](https://warungpintargroup.atlassian.net/browse/NV-1166): \[TS\] Cart

## Collaborate

(Optional - Mention who are the authors of this MR, if there are 2 authors or more. example:
This refactor ts was migrated by collaborative engineering between @ahmad.muzakki and @audy.wisuda
And tested by: Bayu Prakoso)

## Related Documents

(Optional - List documents related of this MR)

## Screenshoot

(Optional - to make engineers easy to understand what's the result of the changes, please embed the screenshot of the result. better if you could embed with Before and After screenshoot. example:

<table>
   <tr>
    <td> <b>Before</b> </td>
    <td> <b>After</b> </td>
   </tr>
 <tr>
  <td><img src="https://gitlab.warungpintar.co/frontend/juragan-mobile/uploads/8717001e62d37c5f637cfa324a30ff88/IMG_20220422_160349.jpg" width="220" height="400" /> </td>
    <td><img src="https://gitlab.warungpintar.co/frontend/juragan-mobile/uploads/a6dbd214e744e3f19c327eba33db179d/qweqeqe.png" width="220" height="400" /> </td>
   </tr>
</table>
)
